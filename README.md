# Práctica1-RedesDeComputadoras

Implementar un servidor de correo básico a través de un script en Python y mediante el uso del módulo de socket.


## Requerimientos

| Lenguaje | Versión |
|:--------:|:-------:|
|  Python  |  3.10   |

## Uso

1. Clonar el repositorio con el comando `git clone https://gitlab.com/regina.gaco/practica1-redesdecomputadoras.git`
2. Ejecutar el servidor con el comando `python3.10 src/Server.py`
3. Desde otra terminal, ejecutar el archivo principal con el comando `python3.10 main.py correo@dominio.com -s asunto`
4. Una vez que se ejecutó el archivo principal, el cliente puede hacer uso de los siguientes comandos:
    - HELO, para iniciar la comunicación con el servidor.
    - MAIL FROM: \<cuenta@dominio.com\>, para indicar el emisor del mensaje.
    - RCPT TO: \<destino@dominio.com\>, para indicar el receptor del mensaje.
    - DATA, para indicar el inicio del mensaje, el cual finalizará cuando haya una línea solo con un punto.
    - QUIT, para terminar la comunicación con el servidor.
