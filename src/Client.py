import socket

from Server import SERVER_PORT, BUF_SIZE
from enums import Message, Code

# Obtenemos el host.
HOST = socket.gethostbyname(socket.gethostname())


def log_foreign_msg(msg):
    """Imprime un mensaje en la consola pero formateado.

    Agrega un emoji de kiwi al inicio del mensaje a imprimir para identificar
    los mensajes del servidor con ese emoji.

    Parameters
    ----------
    msg : str
        El mensaje a imprimir
    """

    print(f"🥝 {msg}")


def log_client_msg(msg):
    """Imprime un mensaje en la consola pero formateado.

    Agrega un emoji de cerezas al inicio del mensaje a imprimir para identificar
    los mensajes cliente con ese emoji.

    Parameters
    ----------
    msg : str
        El mensaje a imprimir
    """

    print(f"🍒 {msg}")


class Client:
    """
    Clase que representa a un cliente.

    ...

    Attributes
    ----------
    __socket : socket object
        El socket del cliente para comunicarse con el servidor.
    __is_sending_data: bool
        El cliente está enviando mensajes y no espera respuesta hasta un '.'.
    __mail: str
        El correo del cliente.

    Methods
    -------
    connect()
        Establece la conexión con el servidor.
    send(*data)
        Envía un mensaje al servidor y obtiene su respuesta.
    """

    def __init__(self, mail):
        # Instanciar la clase socket
        self.__socket = socket.socket()
        self.__is_sending_data = False
        self.__mail = mail
        print("🍒 client ready\n")

    def connect(self):
        """Establece la conexión con el servidor.

        Intenta conectarse con el socket del servidor y si la conexión es exitosa,
        manda el mensaje HELO, pero si no, entonces se imprime en la pantalla un
        mensaje de error.

        Returns
        -------
        bool
            True si la conexión fue exitosa, False en otro caso.
        """

        try:
            # Nos conectamos al socket
            self.__socket.connect((HOST, SERVER_PORT))
            self.send(Message.HELO, HOST)
            self.send(Message.MAIL_FROM, f"<{self.__mail}>")
            return True
        except ConnectionRefusedError:
            log_foreign_msg("unable to connect with the server, run Server.py first")

    def send(self, *data):
        """Envía un mensaje al servidor y obtiene su respuesta.

        Envía un mensaje, transformado a bytes, al servidor y después obtiene la
        respuesta para saber si debe cerrar la conexión o no.

        Parameters
        ----------
        *data : str
            La información del mensaje a enviar.

        Returns
        -------
        bool
            Si debemos cerrar la conexión o no.
        """

        request = " ".join(data)
        log_client_msg(request)
        # Enviamos un mensaje, transformando el string en un objeto de tipo bytes
        self.__socket.send(request.encode())

        has_type_end = request == '.'

        # if the should stop our body
        if self.__is_sending_data and has_type_end:
            self.__is_sending_data = False

        if self.__is_sending_data:
            return False

        # respondemos a la respuesta
        server_message = self.__socket.recv(BUF_SIZE).decode()
        log_foreign_msg(server_message)

        should_close = Code.BYE.name in server_message
        self.__is_sending_data = request.upper() == Message.DATA

        if should_close:
            self.__socket.close()
        return should_close
