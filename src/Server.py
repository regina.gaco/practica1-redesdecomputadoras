import socket
import re

from enums import Message, Code
from src.save_mail import save_mail

BUF_SIZE = 1024
# Definir el puerto a la escucha
SERVER_PORT = 10000

host = socket.gethostbyname(socket.gethostname())


def parse_command(msg: str):
    match msg.split(" "):
        case [command, *data] if command.upper() in [Message.HELO, Message.QUIT, Message.DATA]:
            return [command.upper(), *data]
        case [command_a, command_b, *data] if f"{command_a}_{command_b}".upper() in [Message.RCPT_TO,
                                                                                     Message.MAIL_FROM]:
            return [f"{command_a}_{command_b}".upper(), *data]
        case other:
            return other


class Server:
    """
    Clase que representa a un servidor.

    ...
    
    Attributes
    ----------
    __client : socket object
        El socket del cliente para comunicarse con el servidor.
    __socket : socket object
        El socket del servidor para comunicarse con el cliente.

    Methods
    -------
    send(*data)
        Envía un mensaje al cliente.
    start()
        Inicializa el servidor y queda a la escucha para atender peticiones.
    """

    def __init__(self):
        # Instanciar la clase socket
        self.__client_mail = ''
        self.__client_recipient_mail = ''
        self.__client: socket = None
        self.__socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Enlazar el socket a la interfaz (configuración)
        self.__socket.bind((host, SERVER_PORT))

    def send(self, *data):
        """Envía un mensaje al cliente

        Parameters
        ----------
        *data : str | Code
            La información del mensaje a enviar.
        """

        self.__client.send(" ".join(f"{e}" for e in data).encode())

    def start(self):
        """Inicializa el servidor y queda a la escucha para atender peticiones.

        El servidor maneja las siguientes peticiones: HELO, MAIL FROM, RCPT TO,
        DATA, QUIT. En cualquier otro caso envía el código 500 e informa al cliente
        del error.
        """

        print("🥝 server ready")
        # Establecemos el modo escucha
        self.__socket.listen(5)

        # Indicamos que aceptamos conexiones del mundo exterior,
        # y esperamos conexiones entrantes
        (new_client, address) = self.__socket.accept()
        self.__client = new_client

        # Mostramos en terminal que hemos recibido una nueva conexión
        print(f"🥳 new client {address=}")

        # Declaramos una expresión regular para determinar si un correo es válido
        expresion_regular = re.compile(r'''(
        ^<[a-zA-Z0-9._%+-]+
        @
        [a-zA-Z0-9.-]+
        (\.[a-zA-Z]{2,4})>$)''', re.VERBOSE)

        end_data_delimiter = "."
        is_writing_body = False
        message_in_parts: list[str] = []

        while True:

            # Una vez que se lleva a cabo una conexión
            # preparamos el buffer de datos para recibir información
            msg = new_client.recv(BUF_SIZE).decode()
            request = parse_command(msg)
            match request:
                case [Message.HELO, port]:
                    print(f"🥝 client connected with {port=}")
                    self.send(Code.OK, Code.OK.name)

                case [Message.DATA]:
                    self.send(Code.START_MAIL_INPUT, f"End data with <CR><LF>{end_data_delimiter}<CR><LF>")
                    is_writing_body = True

                case ['.'] if is_writing_body:
                    save_mail(self.__client_mail, self.__client_recipient_mail, _data='\n'.join(message_in_parts))
                    self.send(Code.OK, Code.OK.name)
                    is_writing_body = False

                case [*body_single_line] if is_writing_body:
                    message_in_parts.append(' '.join(body_single_line))

                case [Message.RCPT_TO, mail_to]:
                    if re.search(expresion_regular, mail_to):
                        self.__client_recipient_mail = mail_to
                        self.send(Code.OK, Code.OK.name)
                    else:
                        self.send(Code.NO_SUCH_USER_HERE, "NO_SUCH_USER_HERE")

                case [Message.MAIL_FROM, mail_from]:
                    if re.search(expresion_regular, mail_from):
                        self.__client_mail = mail_from
                        self.send(Code.OK, Code.OK.name)
                    else:
                        self.send(Code.BAD_REQUEST, "BAD_REQUEST")

                case [Message.QUIT]:
                    self.send(Code.BYE, Code.BYE.name)
                    break

                case bad_command:
                    print(f"{bad_command=}")
                    self.send(Code.COMMAND_NOT_FOUND,
                              f"Command not valid, check your syntax, got: {' '.join(f'{e}' for e in bad_command)}")


if __name__ == '__main__':
    try:
        Server().start()
    except BrokenPipeError:
        print("🥝 ⚠ connection lost!")
