from enum import IntEnum, Enum


class Message(str, Enum):
    """
    Clase que representa un mensaje.
    """
    HELO = "HELO"
    QUIT = "QUIT"
    DATA = "DATA"
    RCPT_TO = "RCPT_TO:"
    MAIL_FROM = "MAIL_FROM:"


class Code(IntEnum):
    """
    Clase que representa un código.
    """
    OK = 250
    BYE = 221
    END = 354
    COMMAND_NOT_FOUND = 500
    NO_SUCH_USER_HERE = 550
    START_MAIL_INPUT = 354
    BAD_REQUEST = 400
