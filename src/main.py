import sys

from Client import Client


def run_prompt(_mail, _subject):
    """Maneja la comunicación entre el cliente y el servidor.

    Conecta al cliente con el servidor y lo mantiene así hasta recibir el
    booleano que indica que debe cerrarse la conexión.

    Parameters
    ----------
    _mail : str
        El correo del cliente.
    _subject : str
        El asunto del correo.
    """
    print(f"{_mail=} {_subject=}")
    client = Client(mail)
    connected = client.connect()
    if not connected:
        return

    while True:
        should_stop = client.send(input("~> ").strip())
        if should_stop:
            break


if __name__ == '__main__':
    match sys.argv[1:]:
        case [mail, "-s", subject]:
            run_prompt(mail, subject)
        case _:
            print("pattern required, expect: foo@mail.com -s subject")
