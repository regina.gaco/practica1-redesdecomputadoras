import json;
import os.path;

def save_mail(_from, _to, _data):
    """Guarda la información de un mensaje en el archivo /home/linux/.mailbox_redes

    Crea un objeto de tipo JSON para guardar la información del mensaje y luego
    crea el directorio /home/linux, si es que no existe, para poder guardar la 
    información en el archivo /home/linux/.mailbox_redes

    Parameters
    ----------
    _from : str
        El emisor del mensaje.
    _to : str
        El receptor del mensaje.
    _data : str
        El contenido del mensaje.
    """

    # Creamos el objeto de tipo JSON.
    mail = {
        "from":_from,
        "to":_to,
        "data":_data
    }

    # Creamos el directorio si es que no existe.
    dir_path = "/home/linux"
    if not os.path.isdir(dir_path):
        os.system("sudo mkdir " + dir_path)
        os.system("sudo chmod 777 " + dir_path)
        
    # Creamos el archivo con la información si es que no existe.
    path_to_file = "/home/linux/.mailbox_redes" 
    
    if os.path.exists(path_to_file) == False:
        mail_file = open(path_to_file, "w+")
        mail_file = json.dump([], mail_file)

    # Si el archivo existe, obtenemos su contenido para agregar el nuevo JSON
    # al arreglo que contiene los demás JSON.
    mail_file = open(path_to_file, "r")
    mail_array = json.load(mail_file)

    mail_array.append(mail)

    mail_file = open(path_to_file, "w")
    mail_file = json.dump(mail_array, mail_file)
    